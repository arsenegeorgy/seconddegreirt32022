from flask import Flask, jsonify


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def get_author():
    rslt = {
        "author": "Arsene",
        "Version": 1.0
    }
    return jsonify(rslt)


@app.route('/discriminant')  # complete cette ligne a fin que les données puissent etre reçu par le verbe POST
def discriminant():
    """
    ce endpoint recupere par post les coef a, b, c
    d'une equation du second degré
    :return: un json contenant la valeur du discriminant
    """
    # 1. recupere les données par post
    # 2. calculer le discriminant
    # 3. monter le JSON contenant les ou la clé de votre choix et comme valeur le discriminant
    return 0  # retourner le json


@app.route('/solution')  # complete cette ligne a fin que les données puissent etre reçu par le verbe GET
def solution():  # complete cette ligne afin que les données puissent etre reçu par get
    """
    ce endpoint recupere par post les coef a, b, c et d (le discriminant)
    d'une equation du second degré
    :return: un json contenant la valeur du discriminant
    """
    # 1. calculer les solutions si le discriminant est superieur ou egale a 0
    # 2. monter la reponse json contenant les solution ave les clé de votre choix
    # 3. si le discriminant est inferieur a 0 un json avec pour valeur "error" accompagner de la
    # clé de votre choix sera retourner

    return 0  # retourner le json



if __name__ == '__main__':
    app.run(host='0.0.0.0')